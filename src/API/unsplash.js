import React from 'react'
import Axios from 'axios'

export default Axios.create({
  baseURL: "https://api.unsplash.com",
  headers:{
    Authorization: 
        'Client-ID d0f55f1c697f6a4a4304f2fb03737c1060f534d606b2e31b52b75669c2acc741'
  }
})