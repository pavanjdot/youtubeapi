
import List from './imageList'
import unsplash from '../API/unsplash'
import React from 'react'
import SearchBar from './SearchBar'
import { throwStatement } from '@babel/types'





class App extends React.Component{
  state = {images : []}
   onFormSumbit = async (term) =>{
  
    const response = await unsplash.get("/search/photos", {
      params: {query: term},
    })

    this.setState({images: response.data.results});
     
    

    
  }

  
  render(){
    
    return(
      <div className = "ui container" style={{marginTop: '20px'}}>
        <SearchBar onSubmit={this.onFormSumbit}/>

        <List images={this.state.images}/>

        
      </div>
    );
  }
}

export default App;