import './imageList.css'
import React from 'react'
import Image from './imageCard'

const imageList = (props) => {
  const displayArr = props.images.map( image => {
    return (

      <Image key={image.id} imagefull={image}/>  
    );
    
  })
  return(<div className="image-list">{displayArr}</div>);
}

export default imageList;