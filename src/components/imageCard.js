import React from 'react'

class imageCard extends React.Component{

  constructor(props){
    super(props)

    this.state={span: 0};
    this.imageRef = React.createRef();

    
  }

  componentDidMount(){
   this.imageRef.current.addEventListener('load', this.setSpan);
  }

  setSpan = ()=>{

    const height =  this.imageRef.current.clientHeight;

    const span =Math.ceil( height/10);

    this.setState({span});

  }

  


  

  render(){
    
    const {description, urls} = this.props.imagefull;
    return(
      <div style={{gridRowEnd: `span ${this.state.span}`}}>

        <img alt={description} 
             src={urls.regular}
             ref={this.imageRef} />


      </div>
    );
  }
}
export default imageCard;